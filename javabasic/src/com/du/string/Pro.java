package com.du.string;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;

public class Pro {

	public void run(){
		try{
			
			
			Properties prot =new Properties();
			InputStream fis=new FileInputStream(new File("./config.properties"));
			prot.load(fis);
			System.out.println(prot.getProperty("tables"));
			
			FileOutputStream fos=new FileOutputStream(prot.getProperty("csvpath"),true);
			System.out.println(prot.getProperty("csvpath"));
			
			
			Class.forName(prot.getProperty("driver"));
			Connection conn=DriverManager.getConnection(prot.getProperty("jdbc"),prot.getProperty("user"),prot.getProperty("password"));
			
			Statement st=conn.createStatement();
			ResultSet rs= st.executeQuery("select * from "+prot.getProperty("tables"));
			ResultSetMetaData rsmd=rs.getMetaData();
			String columnC="";
			for(int i=1;i<=rsmd.getColumnCount();i++){
				columnC=columnC+rsmd.getColumnName(i).toString();
				if(i<rsmd.getColumnCount()){
					columnC=columnC+",";
				}
			}
			fos.write(columnC.getBytes());
			while (rs.next()){
				String ls="";
				for(int i=1;i<=rsmd.getColumnCount();i++){

					ls=ls+rs.getObject(i).toString();
					
					if(i<rsmd.getColumnCount()){
						ls=ls+",";
					}
				}
				fos.write("\r\n".getBytes());
				fos.write(ls.getBytes());
			}
			
			//fos.write("aaa".getBytes());
			fos.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			
		}
	}
	public static void main(String[] args) {
		Pro p=new Pro();
		try{
			p.run();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
